Lite Widget
================

Lite Widget provides the logged in user with basic system information and updates status.

## Screenshot:

![](https://imgur.com/QE6jN4R.png)

## License ![License](https://img.shields.io/badge/license-GPLv2-green.svg)

This project is under the GPLv2 license. Unless otherwise stated in individual files.

## Authors
- [Jerry Bezencon](https://gitlab.com/linuxlite/)
